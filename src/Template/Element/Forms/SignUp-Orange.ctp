<?php
use Cake\Core\Configure;
?>

<?= $this->Form->create('Users', array('url' => '/users/users/register/register')); ?>
    <div class="header header-primary text-center">
        <h4 class="title title-up">Sign Up</h4>
        <div class="social-line">
            <a href="/auth/facebook" class="btn btn-neutral btn-facebook btn-icon btn-icon-mini">
                <i class="fa fa-facebook-square"></i>
            </a>
            <a href="/auth/twitter" class="btn btn-neutral btn-twitter btn-icon">
                <i class="fa fa-twitter"></i>
            </a>
            <a href="/auth/google" class="btn btn-neutral btn-google btn-icon  btn-icon-mini">
                <i class="fa fa-google-plus"></i>
            </a>
        </div>
    </div>
    <div class="content">
        <div class="input-group form-group-no-border">
            <span class="input-group-addon">
                <i class="now-ui-icons users_circle-08"></i>
            </span>
            <input id="username" type="text" class="form-control" placeholder="Username..." required>
        </div>
        <div class="input-group form-group-no-border">
            <span class="input-group-addon">
                <i class="now-ui-icons users_circle-08"></i>
            </span>
            <input id="first_name" type="text" class="form-control" placeholder="First Name..." required>
        </div>
        <div class="input-group form-group-no-border">
            <span class="input-group-addon">
                <i class="now-ui-icons users_circle-08"></i>
            </span>
            <input id="last_name" type="text" class="form-control" placeholder="Last Name..." required>
        </div>
        <div class="input-group form-group-no-border">
            <span class="input-group-addon">
                <i class="now-ui-icons ui-1_email-85"></i>
            </span>
            <input id="email" type="text" class="form-control" placeholder="Email..." required>
        </div>
        <div class="input-group form-group-no-border">
            <span class="input-group-addon">
                <i class="now-ui-icons ui-1_lock-circle-open"></i>
            </span>
            <input id="password" type="password" placeholder="Password..." class="form-control" required/>
        </div>
        <div class="input-group form-group-no-border">
            <span class="input-group-addon">
                <i class="now-ui-icons ui-1_lock-circle-open"></i>
            </span>
            <input id="password_confirm" type="password" placeholder="Password..." class="form-control" required/>
        </div>
        <!-- If you want to add a checkbox to this form, uncomment this code -->
        <!-- <div class="checkbox">
          <input id="checkboxSignup" type="checkbox">
              <label for="checkboxSignup">
              Unchecked
              </label>
            </div> -->

        <?php
        if (Configure::read('Users.Tos.required')) {
            echo $this->Form->control('tos', ['type' => 'checkbox', 'label' => __d('CakeDC/Users', 'Accept TOS conditions?'), 'required' => true]);
        }
        if (Configure::read('Users.reCaptcha.registration')) {
            echo $this->User->addReCaptcha();
        }
        ?>

    </div>
    <div class="footer text-center">
        <button type="submit" class="btn btn-neutral btn-round btn-lg">Get Started</button>
    </div>
<?= $this->Form->end() ?>