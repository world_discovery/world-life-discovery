<?php
use Cake\Core\Configure;
?>


    <div class="header header-primary text-center">
        <h4 class="title title-up">Sign Up</h4>
        <div class="social-line">
            <a href="/auth/facebook" class="btn btn-neutral btn-facebook btn-icon btn-icon-mini">
                <i class="fa fa-facebook-square"></i>
            </a>
            <a href="/auth/twitter" class="btn btn-neutral btn-twitter btn-icon">
                <i class="fa fa-twitter"></i>
            </a>
            <a href="/auth/google" class="btn btn-neutral btn-google btn-icon  btn-icon-mini">
                <i class="fa fa-google-plus"></i>
            </a>
        </div>
    </div>
<div class="content"><h6 class="title title-up text-center">OR</h6></div>
    <div class="footer text-center">
        <a href="users/users/register" class="btn btn-neutral btn-round btn-lg">Create Account</a>
    </div>
