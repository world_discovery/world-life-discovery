<footer class="footer" data-background-color="black">
    <div class="container">
        <nav>
            <ul>
                <li>
                    <a href="#">
                        Item 1
                    </a>
                </li>
                <li>
                    <a href="#">
                        Item 2
                    </a>
                </li>
                <li>
                    <a href="#">
                        Item 3
                    </a>
                </li>
                <li>
                    <a href="#">
                        Item 4
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>, World Life Discovery.
        </div>
    </div>
</footer>