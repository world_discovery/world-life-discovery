<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'World Life Discovery - Epic World Events';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'/>
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->meta(
        'keywords',
        'enter any meta keyword here'
    );
    ?>

    <?= $this->Html->meta('icon') ?>

    <!-- CDN LINKS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- LOCAL RESOURCES -->
    <!-- //$this->Html->css('base.css') -->
    <?= $this->Html->css('cake.css') ?>

    <!-- UI KIT -->
    <?= $this->Html->css('/assets/css/bootstrap.min.css', array('inline' => false)) ?>
    <?= $this->Html->css('/assets/css/now-ui-kit.css', array('inline' => false)) ?>
    <?= $this->Html->css('/assets/css/helper.css', array('inline' => true)) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css'); ?>
    <?= $this->fetch('script'); ?>

</head>

<body class="index-page">

<!-- Navbar -->
<?= $this->element('/navbars/top_bar'); ?>
<!-- End Navbar -->

<div class="wrapper">
    <div class="page-header clear-filter" filter-color="orange">
        <div class="page-header-image" data-parallax="true" style="background-image: url('/assets/img/header.jpg');">
        </div>
        <div class="container">
            <div class="content-center brand">
                <img class="n-logo" src="/assets/img/now-logo.png" alt="">
                <h1 class="h1-seo">World Life Discovery</h1>
                <h3>Muita manheta</h3>
            </div>
        </div>
    </div>
    <div class="main">
       <?= $this->fetch('content'); ?>
    </div>

    <!-- Footer -->
    <?= $this->element('footer'); ?>

</div>



    <?= $this->Flash->render(); ?>
<!--

-->

<!--   Core JS Files   -->
<?= $this->Html->script('/assets/js/core/jquery.3.2.1.min.js') ?>
<?= $this->Html->script('/assets/js/core/tether.min.js') ?>
<?= $this->Html->script('/assets/js/core/bootstrap.min.js') ?>

<?= $this->Html->script('/assets/js/plugins/bootstrap-switch.js') ?>
<?= $this->Html->script('/assets/js/plugins/nouislider.min.js') ?>

<?= $this->Html->script('/assets/js/plugins/bootstrap-datepicker.js') ?>
<?= $this->Html->script('/assets/js/now-ui-kit.js') ?>

<script type="text/javascript">
    $(document).ready(function() {
        // initiali UI Kit Sliders
        // nowuiKit.initSliders();

        /* off-canvas sidebar toggle */
        $('[data-toggle=offcanvas]').click(function() {
            $('.row-offcanvas').toggleClass('active');
            $('.collapse').toggleClass('in').toggleClass('hidden-xs').toggleClass('visible-xs');
        });

    });


</script>


</body>


</html>
