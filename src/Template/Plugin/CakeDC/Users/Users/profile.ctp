<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>

    <div class="row row-offcanvas row-offcanvas-left">
        <!-- sidebar -->
        <div class="column col-sm-3 col-xs-1 sidebar-offcanvas" id="sidebar">
            <ul class="nav" id="menu">
                <li><a href="/profile"><i class="fa fa-2x fa-user"></i> <span class="collapse in hidden-xs">Profile</span></a></li>
                <li><a href="#"><i class="fa fa-2x fa-list"></i> <span class="collapse in hidden-xs">Affiliates</span></a></li>
                <li><a href="#"><i class="fa fa-2x fa-paperclip"></i> <span class="collapse in hidden-xs">Tickets</span></a></li>
                <li><a href="#"><i class="fa fa-2x fa-eye"></i> <span class="collapse in hidden-xs">Events</span></a></li>
                <li><a href="#"><i class="fa fa-2x fa-refresh"></i> <span class="collapse in hidden-xs">Buy Tickets</span></a></li>

                <div class="separator"></div>
                <li><a href="/logout"><i class="fa fa-2x fa-exit"></i> <span class="collapse in hidden-xs">Logout</span></a></li>
            </ul>
        </div>
        <!-- /sidebar -->

        <!-- main right col -->
        <div class="column col-sm-9 col-xs-11" id="main">
            <p><a data-toggle="offcanvas"><i class="fa fa-navicon fa-2x"></i></a></p>
            <p>

                <div class="users">
                    <h3><?= $this->Html->image(
                            empty($user->avatar) ? $avatarPlaceholder : $user->avatar,
                            ['width' => '180', 'height' => '180']
                        ); ?></h3>
                    <h3>
                        <?=
                        $this->Html->tag(
                            'span',
                            __d('CakeDC/Users', '{0} {1}', $user->first_name, $user->last_name),
                            ['class' => 'full_name']
                        )
                        ?>
                    </h3>
                    <?php //@todo add to config ?>
                    <?= $this->Html->link(__d('CakeDC/Users', 'Change Password'), ['plugin' => 'CakeDC/Users', 'controller' => 'Users', 'action' => 'changePassword']); ?>
                    <div class="row">
                        <div class="large-6 columns strings">
                            <h6 class="subheader"><?= __d('CakeDC/Users', 'Username') ?></h6>
            <p><?= h($user->username) ?></p>
            <h6 class="subheader"><?= __d('CakeDC/Users', 'Email') ?></h6>
            <p><?= h($user->email) ?></p>
            <?= $this->User->socialConnectLinkList($user->social_accounts) ?>
            <?php
            if (!empty($user->social_accounts)):
                ?>
                <h6 class="subheader"><?= __d('CakeDC/Users', 'Social Accounts') ?></h6>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th><?= __d('CakeDC/Users', 'Avatar'); ?></th>
                        <th><?= __d('CakeDC/Users', 'Provider'); ?></th>
                        <th><?= __d('CakeDC/Users', 'Link'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($user->social_accounts as $socialAccount):
                        $escapedUsername = h($socialAccount->username);
                        $linkText = empty($escapedUsername) ? __d('CakeDC/Users', 'Link to {0}', h($socialAccount->provider)) : h($socialAccount->username)
                        ?>
                        <tr>
                            <td><?=
                                $this->Html->image(
                                    $socialAccount->avatar,
                                    ['width' => '90', 'height' => '90']
                                ) ?>
                            </td>
                            <td><?= h($socialAccount->provider) ?></td>
                            <td><?=
                                $this->Html->link(
                                    $linkText,
                                    $socialAccount->link,
                                    ['target' => '_blank']
                                ) ?></td>
                        </tr>
                        <?php
                    endforeach;
                    ?>
                    </tbody>
                </table>
                <?php
            endif;
            ?>
        </div>
    </div>
</div>

            </p>
        </div>
        <!-- /main -->


