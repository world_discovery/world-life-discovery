<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Error\Debugger;
use Cake\Core\Configure;
$this->layout = 'default';
?>

<div class="section section-about-us">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 text-center">
                <h2 class="title">Who we are?</h2>
                <h5 class="description">According to the National Oceanic and Atmospheric Administration, Ted, Scambos, NSIDClead scentist, puts the potentially record low maximum sea ice extent tihs year down to low ice extent in the Pacific and a late drop in ice extent in the Barents Sea.</h5>
            </div>
        </div>
        <div class="separator separator-primary"></div>
        <div class="section-story-overview">
            <div class="row">
                <div class="col-md-6">
                    <!-- First image on the left side -->
                    <div class="image-container image-left" style="background-image: url('../assets/img/bg38.jpg')">
                        <p class="blockquote blockquote-primary">"Over the span of the satellite record, Arctic sea ice has been declining significantly, while sea ice in the Antarctichas increased very slightly"
                            <br>
                            <br>
                            <small>-NOAA</small>
                        </p>
                    </div>
                    <!-- Second image on the left side of the article -->
                    <div class="image-container image-left-bottom" style="background-image: url('../assets/img/bg24.jpg')"></div>
                </div>
                <div class="col-md-5">
                    <!-- First image on the right side, above the article -->
                    <div class="image-container image-right" style="background-image: url('../assets/img/bg39.jpg')"></div>
                    <h3>So what does the new record for the lowest level of winter ice actually mean</h3>
                    <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever happens with climate change. Even if the Arctic continues to be one of the fastest-warming regions of the world, it will always be plunged into bitterly cold polar dark every winter. And year-by-year, for all kinds of natural reasons, there’s huge variety of the state of the ice.
                    </p>
                    <p>
                        For a start, it does not automatically follow that a record amount of ice will melt this summer. More important for determining the size of the annual thaw is the state of the weather as the midnight sun approaches and temperatures rise. But over the more than 30 years of satellite records, scientists have observed a clear pattern of decline, decade-by-decade.
                    </p>
                    <p>The Arctic Ocean freezes every winter and much of the sea-ice then thaws every summer, and that process will continue whatever happens with climate change. Even if the Arctic continues to be one of the fastest-warming regions of the world, it will always be plunged into bitterly cold polar dark every winter. And year-by-year, for all kinds of natural reasons, there’s huge variety of the state of the ice.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section pt-0 pb-0">
    <div class="team-3 section-image" data-parallax="true" style="background-image: url('/assets/img/bg44.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 text-center">
                    <h2 class="title">Our little team.</h2>
                    <h4 class="description">
                        This is the paragraph where you can write more details about your product. Keep you user engaged by providing meaningful information.
                    </h4>
                </div>
                <div class="col-md-6">
                    <div class="card card-profile">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="card-image">
                                    <a href="#pablo">
                                        <img class="img" src="/assets/img/eva.jpg">
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="card-block">
                                    <h3 class="card-title">Ariana Hazel</h3>
                                    <h6 class="category text-primary"> Fashion Designer</h6>
                                    <p class="card-description">
                                        Happiness resides not in possessions, and not in gold, happiness dwells in the soul...
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-profile">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="card-image">
                                    <a href="#pablo">
                                        <img class="img" src="/assets/img/eva.jpg">
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="card-block">
                                    <h3 class="card-title">Ryan Samuel</h3>
                                    <h6 class="category text-primary">Financial Examiner</h6>
                                    <p class="card-description">
                                        Today you are you! That is truer than true! There is no one alive who is you-er than you!..
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section section-basic">
    <div class="container">
        <h3 class="title">Basic Elements</h3>
    </div>
</div>

<div class="section" id="carousel">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8">
                <div id="carouselExampleControls" class="carousel slide">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <img class="d-block" src="/assets/img/bg1.jpg" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Nature, United States</h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block" src="/assets/img/bg3.jpg" alt="Second slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Somewhere Beyond, United States</h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block" src="/assets/img/bg4.jpg" alt="Third slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Yellowstone National Park, United States</h5>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <i class="now-ui-icons arrows-1_minimal-left"></i>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <i class="now-ui-icons arrows-1_minimal-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section section-nucleo-icons">
    <div class="container">
        <div class="row">

        </div>
    </div>
</div>
<div class="section">
    <div class="container text-center">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-lg-8">
                <h2 class="title">behind the word mountains</h2>
                <h5 class="description">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</h5>
            </div>
        </div>
    </div>
</div>

<div class="section section-signup" style="background-size: cover; background: url('/assets/img/bg4.jpg') top center;min-height: 700px;">
    <div class="container">

        <div class="row">
            <div class="card card-signup" data-background-color="orange">
                <?= $this->element('/Forms/SignUp-Orange-Small'); ?>
            </div>
        </div>
        <div class="col text-center">
            <a href="/login" class="btn btn-simple btn-round btn-white btn-lg" target="_blank">Login Page</a>
        </div>
    </div>
</div>

<div class="section section-examples" data-background-color="black">
    <div class="space-50"></div>
    <div class="container text-center">
        <div class="row">
            <div class="col">
                <a href="#" class="btn btn-simple btn-primary btn-round">manheta 1</a>
            </div>
            <div class="col">
                <a href="#" class="btn btn-simple btn-primary btn-round">manheta 2</a>
            </div>
        </div>
    </div>
</div>

<div class="section section-download" id="#download-section" data-background-color="black">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="text-center col-md-12 col-lg-8">
                <h3 class="title">Do you love travelling?</h3>
                <h5 class="description">Cause if you do, it can be yours for FREE. Hit the button  to navigate to register page where you can find more info.</h5>
            </div>
            <div class="text-center col-md-12 col-lg-8">
                <a href="#" class="btn btn-primary btn-lg btn-round" role="button">
                    Register
                </a>
                <a href="#" target="_blank" class="btn btn-primary btn-lg btn-simple btn-round" role="button">
                    Know More
                </a>
            </div>
        </div>
        <br>
        <br>
        <div class="row justify-content-md-center sharing-area text-center">
            <div class="text-center col-md-12 col-lg-8">
                <h3>World Life Discovery</h3>
            </div>
            <div class="text-center col-md-12 col-lg-8">
                <a target="_blank" href="#" class="btn btn-neutral btn-icon btn-twitter" rel="tooltip" title="Follow us">
                    <i class="fa fa-twitter"></i>
                </a>
                <a target="_blank" href="#" class="btn btn-neutral btn-icon btn-facebook" rel="tooltip" title="Like us">
                    <i class="fa fa-facebook-square"></i>
                </a>
                <a target="_blank" href="#" class="btn btn-neutral btn-icon btn-linkedin" rel="tooltip" title="Follow us">
                    <i class="fa fa-linkedin"></i>
                </a>
            </div>
        </div>
    </div>
</div>
