# WORLD LIFE DISCOVERY

[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![License](https://img.shields.io/packagist/l/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

Main Repository for WLD V0.1.0

The development website can be found here: [World Life Discovery](http://wld.gocherry.pt).
`Development builds are only pushed online at version releases.`

```bash
After login the profile page is available
```
[/profile](http://wld.gocherry.pt/profile)


## Installation

1. Update [Composer](https://getcomposer.org/doc/00-intro.md) run `composer update`.
2. Skip Run `php composer.phar create-project --prefer-dist cakephp/app [app_name]`.

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8080
```

Then visit `http://localhost:8080` to see the page.

The datasource is set to Remote from web-hosting, changes to DB have to be made there.

## Update
```bash
Careful with Edited vendor files
```

## Configuration

Read and edit `config/app.php` and setup the `'Datasources'` and any other
configuration relevant for the application.

```bash
vendor/cakedc/users/config/users.php
```

## Layout

The app skeleton uses a subset of [Foundation](http://foundation.zurb.com/) CSS
framework by default, overwriten with bootstrap 4.


## .gitignore
Comment /config/app.php
`#/config/app.php`

 Remove Ignores from SubFolders: 
1. `!/config/app.php`
2. `!/vendor/cakedc/*`
3. `!vendor/cakephp/src/Utility/Security.php` 